'use strict'

let isGrayModeOn = false;

window.onload = () => {
    addEventListenerForAudioInput();
    addEventListenerForImageInput();
    addEventListenerForVideoInput();
}

function showUserVideo() {
    let divExample = document.getElementById("getUserMediaExample");
    showVideoHelper(divExample, 'Пробвай getUserMedia().');
}


function showVideoForSnapshot() {
    let divExample = document.getElementById("getSnapshotExample");
    showVideoHelper(divExample, 'Кликни тук ако искаш да си направиш селфи.');
    let takeSnapshotButton = document.createElement('button');
    takeSnapshotButton.id = 'takeSnapshotButton';
    takeSnapshotButton.innerText = 'Направи снимка.';
    takeSnapshotButton.className = 'flexButton';
    takeSnapshotButton.onclick = () => {
        event.preventDefault();
        takeSnapshotButton.disabled = true;
        takeSnapshotButton.innerText = 'Изчакай 2 секунди.'
        setTimeout(() => {
            takeSnapshot();
            takeSnapshotButton.disabled = false;
            takeSnapshotButton.innerText = 'Направи снимка.';
            }, 2000);
    }
    divExample.insertBefore(takeSnapshotButton, document.getElementById('hideVideoButton'));
}

function takeSnapshot() {
    if (document.getElementById('photoCanvas')) {
        document.getElementById('photoCanvas').remove();
    }

    let removeImageButton = document.createElement('button');
    removeImageButton.id = 'removeImageButton';
    removeImageButton.innerText = 'Изтрий снимката.';
    removeImageButton.className = 'flexButton';
    removeImageButton.onclick = () => {
        event.preventDefault();
        document.getElementById('userPhoto').remove();
        removeImageButton.remove();
    }
    document.getElementById('getSnapshotExample').appendChild(removeImageButton);

    const canvas = document.createElement('canvas');
    const video = document.getElementById('userVideo');
    const width = video.videoWidth;
    const height = video.videoHeight;
    const context = canvas.getContext('2d');

    canvas.id = 'photoCanvas';
    canvas.width = width;
    canvas.height = height;

    context.drawImage(video, 0, 0, width, height);

    let imageDataURL = canvas.toDataURL('image/png');
    let image = new Image();
    image.id = 'userPhoto';
    image.style = video.style;
    image.setAttribute('src', imageDataURL);

    if (isGrayModeOn) {
        css(image, {
            '-webkit-filter': 'grayscale(1)'
        });
    }

    document.getElementById('getSnapshotExample').appendChild(image);

}

function showVideoHelper(parentDocument, buttonText) {
    let video = document.createElement("video");
    video.autoplay = true;
    video.id = 'userVideo';
    parentDocument.appendChild(video);
    parentDocument.querySelector('button').remove();

    if (navigator.mediaDevices.getUserMedia) {
        navigator.mediaDevices.getUserMedia({ video: true })
            .then((stream) => {
                video.srcObject = stream;
            })
            .catch((err) => {
                console.log(err);
            });
        let hideVideoButton = document.createElement('button');
        hideVideoButton.id = 'hideVideoButton'
        hideVideoButton.className = 'flexButton';
        hideVideoButton.innerText = 'Скрий видеото.';
        hideVideoButton.onclick = () => {
            event.preventDefault();
            hideUserVideo(parentDocument, buttonText);
        }
        parentDocument.appendChild(hideVideoButton);
        const makeVideoGrayButton = document.createElement('button');
        makeVideoGrayButton.id = 'makeVideoGrayButton';
        makeVideoGrayButton.className = 'flexButton';
        makeVideoGrayButton.innerText = 'Добави черно-бял филтър.'
        makeVideoGrayButton.onclick = () => {
            event.preventDefault();
            makeVideoGray();
            makeVideoGrayButton.remove();
            const clearFilterButton = document.createElement('button');
            clearFilterButton.id = 'clearFilterButton';
            clearFilterButton.className = 'flexButton';
            clearFilterButton.innerText = 'Изчисти филтъра.'
            clearFilterButton.onclick = () => {
                event.preventDefault();
                clearFilter();
                clearFilterButton.remove();
                parentDocument.appendChild(makeVideoGrayButton);
            };
            parentDocument.appendChild(clearFilterButton);
        };

        parentDocument.appendChild(makeVideoGrayButton);
    }
}

function makeVideoGray() {
    const videoElement = document.getElementById('userVideo');
    css(videoElement, {
        '-webkit-filter': 'grayscale(1)'
    });
    isGrayModeOn = true;
}

function css(element, style) {
    for (const property in style) {
        element.style[property] = style[property];
    }
}

function clearFilter() {
    const videoElement = document.getElementById('userVideo');
    videoElement.style.webkitFilter = '';
    isGrayModeOn = false;
}

function hideUserVideo(parentDocument, buttonText) {
    const elements = [
        document.getElementById('userVideo'),
        document.getElementById('hideVideoButton'),
        document.getElementById('makeVideoGrayButton'),
        document.getElementById('clearFilterButton'),
        document.getElementById('takeSnapshotButton'),
        document.getElementById('removeImageButton'),
        document.getElementById('userPhoto')];

    if (!elements[0] || !elements[1]) {
        return;
    }
    for (const element of elements) {
        if (element) {
            element.remove();
        }
    }

    let showVideoButton = document.createElement('button');
    showVideoButton.id = 'showVideoButton';
    showVideoButton.className = 'flexButton';
    showVideoButton.innerText = buttonText;
    showVideoButton.onclick = () => {
        event.preventDefault();
        if (parentDocument.id === 'getSnapshotExample') {
            showVideoForSnapshot();
        } else {
            showUserVideo();
        }
    }

    parentDocument.appendChild(showVideoButton);

}

function addEventListenerForAudioInput() {
    const AUDIO_INPUT_ID = 'audio';
    const AUDIO_OUTPUT_ID = 'audioOutput';

    let audioInput = document.getElementById(AUDIO_INPUT_ID);
    let audioOutput = document.getElementById(AUDIO_OUTPUT_ID);

    audioInput.addEventListener('change', (e) => {
        let file = audioInput.files[0];
        const audioType = /audio.*/;

        if (!file) {
            let uploadedAudio = document.getElementById('uploadedAudio');
            uploadedAudio.remove();
            return;
        }

        if (file.type.match(audioType)) {
            let reader = new FileReader();

            reader.onload = (e) => {
                audioOutput.innerHTML = "";

                let audioElement = new Audio();
                audioElement.src = reader.result;
                audioElement.id = 'uploadedAudio';
                audioElement.controls = true;

                audioOutput.appendChild(audioElement);
            }

            reader.readAsDataURL(file);
        } else {
            audioOutput.innerHTML = "File not supported!"
        }
    });
}

function addEventListenerForImageInput() {
    const IMAGE_INPUT_ID = 'image';
    const IMAGE_OUTPUT_ID = 'imageOutput';

    let imageInput = document.getElementById(IMAGE_INPUT_ID);
    let imageOutput = document.getElementById(IMAGE_OUTPUT_ID);

    imageInput.addEventListener('change', (e) => {
        let file = imageInput.files[0];
        const imageType = /image.*/;

        if(!file) {
            let uploadedImage = document.getElementById('uploadedImage');
            uploadedImage.remove();
            let imageCaption = document.getElementById('uploadedImageCaption');
            imageCaption.remove();
            return;
        }

        if (file.type.match(imageType)) {
            let reader = new FileReader();

            reader.onload = (e) => {
                imageOutput.innerHTML = "";

                let imageElement = new Image();
                imageElement.src = reader.result;
                imageElement.id = 'uploadedImage';

                imageOutput.appendChild(imageElement);

                let imageCaption = document.createElement('figcaption');
                imageCaption.id = 'uploadedImageCaption';
                imageCaption.innerText = 'Uploaded photo';

                imageOutput.appendChild(imageCaption);
            }

            reader.readAsDataURL(file);
        } else {
            imageOutput.innerHTML = "File not supported!"
        }
    });
}

function addEventListenerForVideoInput() {
    const VIDEO_INPUT_ID = 'video';
    const VIDEO_OUTPUT_ID = 'videoOutput';

    let videoInput = document.getElementById(VIDEO_INPUT_ID);
    let videoOutput = document.getElementById(VIDEO_OUTPUT_ID);

    videoInput.addEventListener('change', (e) => {
        let file = videoInput.files[0];
        const videoType = /video.mp4/;

        if(!file) {
            let uploadedVideo = document.getElementById('uploadedImage');
            uploadedVideo.remove();
            let videoCaption = document.getElementById('uploadedImageCaption');
            videoCaption.remove();
            return;
        }

        if (file.type.match(videoType)) {
            let reader = new FileReader();

            reader.onload = (e) => {
                videoOutput.innerHTML = "";

                let videoElement = document.createElement('video');
                videoElement.src = reader.result;
                videoElement.id = 'uploadedImage';
                videoElement.controls = true;
                videoElement.autoplay = true;
                videoElement.width = 600;
                videoElement.height = 400;

                videoOutput.appendChild(videoElement);

                let videoCaption = document.createElement('figcaption');
                videoCaption.id = 'uploadedImageCaption';
                videoCaption.innerText = 'Uploaded video';

                videoOutput.appendChild(videoCaption);
            }

            reader.readAsDataURL(file);
        } else {
            videoOutput.innerHTML = "File not supported!"
        }
    });
}